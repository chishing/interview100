package com.zhss.order.service;

import com.zhss.inventory.api.InventoryApi;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(value = "inventory-service")
public interface InventoryService extends InventoryApi {
}
