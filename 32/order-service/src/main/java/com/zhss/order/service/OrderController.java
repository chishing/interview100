package com.zhss.order.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private InventoryService inventoryService;

    @Autowired
    private WmsService wmsService;

    @Autowired
    private CreditService creditService;

    /**
     * 没有网关的时候 :去这里请求就可以 http://localhost:9090/order/create?productId=1&userId=10086&count=10&totalPrice=1000
     *
     * @param productId  商品ID
     * @param userId     用户ID
     * @param count      购买数量
     * @param totalPrice 增加多少积分
     * @return
     */
    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public String greeting(@RequestParam("productId") Long productId, @RequestParam("userId") Long userId
            , @RequestParam("count") Long count, @RequestParam("totalPrice") Long totalPrice) {
        System.out.println("创建订单");
        inventoryService.deductStock(productId, count);
        // wmsService.delivery(productId);
        // creditService.add(userId, totalPrice);
        return "success";
    }
}
