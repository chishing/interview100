package com.zhss.order.service;

import com.zhss.wms.service.WmsApi;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(value = "wms-service")
public interface WmsService extends WmsApi {
}
