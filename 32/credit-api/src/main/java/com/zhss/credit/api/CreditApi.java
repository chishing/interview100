package com.zhss.credit.api;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 积分微服务
 *
 * @author
 */
@RequestMapping("/credit")
public interface CreditApi {

    /**
     * gei某个用户增加xx个积分
     *
     * @param userId
     * @param credit 积分
     * @return
     */
    @RequestMapping(value = "/add/{userId}/{credit}", method = RequestMethod.PUT)
    String add(@PathVariable("userId") Long userId, @PathVariable("credit") Long credit);
}
