package com.zhss.wms.service;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@RequestMapping("/wms")
public interface WmsApi {

    /**
     * 发货, 哈哈
     * @param productId
     * @return
     */
    @RequestMapping(value = "/delivery/{productId}", method = RequestMethod.PUT)
    String delivery(@PathVariable("productId") Long productId);

}
