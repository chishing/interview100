package com.zhss.wms.service;

import org.springframework.web.bind.annotation.RestController;

@RestController
public class WmsService implements WmsApi {
    /**
     * 发货, 哈哈
     *
     * @param productId
     * @return
     */
    @Override
    public String delivery(Long productId) {
        System.out.printf("已经安排订单号: %d发货\n",productId);
        return "success";
    }
}
