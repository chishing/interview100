package com.zhss.inventory.service;

import com.zhss.inventory.api.InventoryApi;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

@RestController
public class InventoryService implements InventoryApi {
    /**
     * 扣除库存的,哈哈
     *
     * @param productId
     * @param stock
     * @return
     */
    @Override
    public String deductStock(Long productId, Long stock) {
        System.out.println("对商品【productId=" + productId + "】扣减库存：" + stock);
        try {
            // 模拟超时
            TimeUnit.SECONDS.sleep(0b110L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "{'msg': 'success'}";
    }
}
