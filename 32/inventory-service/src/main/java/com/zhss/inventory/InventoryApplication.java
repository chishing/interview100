package com.zhss.inventory;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * 库存微服务
 *
 *
 */
@SpringBootApplication
@EnableEurekaClient
public class InventoryApplication
{
    public static void main( String[] args )
    {
        SpringApplication.run(InventoryApplication.class,args);
    }
}
