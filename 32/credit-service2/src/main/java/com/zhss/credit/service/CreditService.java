package com.zhss.credit.service;

import com.zhss.credit.api.CreditApi;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * 积分微服务
 *
 * @author
 */
@RestController
public class CreditService implements CreditApi {

    /**
     * gei某个用户增加xx个积分
     *
     * @param userId
     * @param credit 积分
     * @return
     */
    @Override
    public String add(@PathVariable("userId") Long userId,
                      @PathVariable("credit") Long credit) {
        System.out.println("对用户【userId=" + userId + "】增加积分：" + credit);
        return "{'msg': 'success'}";
    }
}