package com.zhss;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * 积分微服务
 * @author
 */
@SpringBootApplication
@EnableEurekaClient
public class CreditApplication
{
    public static void main( String[] args )
    {
        SpringApplication.run(CreditApplication.class,args);
    }
}
