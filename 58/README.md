你们的分布式锁做过高并发优化吗？能抗下每秒上万并发吗？

有很多同学出去面试，聊到分布式锁这块，都被人考察分布式锁能不能抗住高并发的问题了
对某个商品下单，对一个分布式锁每秒突然上万个请求过来，都要进行加锁，此时怎么办呢？
 有时候某个key很可能存放在某个机器上，导致压力很大

比如你的苹果库存有10 000个，此时你在数据库中创建10个库存字段，一个表里存放10个库存字段，stock_1, stock_2， 每个库存字段里放1000个库存
此时这个库存的分布式锁， 对应10个key，product_1_stock_01,product_1_stock_02

请求过来之后，你从10个key随机选择一个key，去加锁就可以了（让这些key均匀分布在不同的机器上）， 每秒过来1万个请求，
此时他们会对10个库存字段key加锁，每个key就1000个请求，每台服务器就1000个去给请求而已，


万一说某个库存分段仅仅剩余10个库存了，此时我下订单要买20个苹果，合并扣减库存，你对product_1_stock_5，加锁了，此时查询对应的数据库中的库存，此时库存是10个
不够买20个苹果，
你可以尝试锁product_1_stock_1,再去查询他的库存可能有30个

此时i就可以下订单，锁定库存的时候，就对product_1_stock_5锁定10个库存，对product_1_stock_1锁定10个库存，锁定20个库存

分段加锁+ 合并扣减