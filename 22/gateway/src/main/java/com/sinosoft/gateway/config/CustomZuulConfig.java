package com.sinosoft.gateway.config;

import com.sinosoft.gateway.event.RefreshRouteService;
import com.sinosoft.gateway.zuul.CustomRouteLocator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.cloud.netflix.zuul.filters.ZuulProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * Created by xujingfeng on 2017/4/1.
 */
@Configuration
public class CustomZuulConfig {

    @Autowired
    ZuulProperties zuulProperties;
    @Autowired
    ServerProperties server;
    @Autowired
    JdbcTemplate jdbcTemplate;

    /**
     * 自定义一个动态路由加载器,,主要是继承了SimpleRouteLocator 实现了 RefreshableRouteLocator
     * zuul的源码也是这么做的,具体可以看这个源码{@link org.springframework.cloud.netflix.zuul.filters.discovery.DiscoveryClientRouteLocator}
     * 思路就是:定时查询数据库中的路由规则, 然后更新到一个单例的SimpleRouteLocator的private AtomicReference<Map<String, ZuulRoute>> routes = new AtomicReference<>();
     * 这个AtomicReference是线程安全的, 至于SimpleRouteLocator是怎么变成单例的可以看这里,{@link org.springframework.cloud.netflix.zuul.ZuulServerAutoConfiguration#simpleRouteLocator()}
     * 从这个也可以看到源码底层实现动态路由使用的是SimpleRouteLocator
     *
     * 至于什么时候粗发这个更新路由呢,可以看这里{@link RefreshRouteService#refreshRoute()}
     * @return
     */
    @Bean
    public CustomRouteLocator routeLocator() {
        // CustomRouteLocator routeLocator = new CustomRouteLocator(this.server.getServletPrefix(), this.zuulProperties);
        CustomRouteLocator routeLocator = new CustomRouteLocator("", this.zuulProperties);
        routeLocator.setJdbcTemplate(jdbcTemplate);
        return routeLocator;
    }

}
