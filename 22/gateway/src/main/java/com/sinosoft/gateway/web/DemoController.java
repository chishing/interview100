package com.sinosoft.gateway.web;

import com.sinosoft.gateway.event.RefreshRouteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.zuul.web.ZuulHandlerMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * Created by xujingfeng on 2017/4/1.
 */
@RestController
public class DemoController {



    /**
     * 因为这里我使用定时器去更新路由{@link RefreshRouteService#refreshRoute() },
     * 所以不用粗发这个controller这手动更新
     @Autowired
     RefreshRouteService refreshRouteService;

     @RequestMapping("/refreshRoute")
     public String refreshRoute() {
     refreshRouteService.refreshRoute();
     return "refreshRoute";
     }
     */

    @Autowired
    ZuulHandlerMapping zuulHandlerMapping;

    /**
     * 查看当前路由信息
     * @return
     */
    @RequestMapping("/watchNowRoute")
    public String watchNowRoute() {
        //可以用debug模式看里面具体是什么
        Map<String, Object> handlerMap = zuulHandlerMapping.getHandlerMap();
        // handlerMap.forEach((k, v) -> {
        //     System.out.print(k);
        //     System.out.print("\t");
        //     System.out.println(v);
        // });
        return "handlerMap";
    }


}
