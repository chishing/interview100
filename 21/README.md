021、你们对网关的技术选型是怎么考虑的？能对比一下各种网关技术的优劣吗？ 

网关的核心功能
1. 动态路由：新开发某服务，动态把请求路径和服务的映射关系热加载到网关里；服务增减机器，网关自动热感知 
2. 灰度发布: 比如新版本上线,可以允许部分流量比如10%的走新版本部署的机器, 
3. 鉴权认证 
4. 性能监控：每个api接口的耗时，成功率，QPS 
5. 系统日志：比如发送过来是什么数据， 返回是什么数据 
6. 数据缓存 
7. 限流熔断 (内置 hystrix和ribbon)
8. 负载均衡 

几种技术选型

Kong，Zuul，Nignx+lua（OpenResty），自研网关

kong：Nignix里边一个基于lua写的模块，实现网关的功能，来实现一些基础功能，比如灰度发现，限流等等。。。
zuul：基于SpringCloud 的架构
Nignx+lua（OpenResty）：
自研网关：自己写类似zuul的网关，基于Servlet，Netty做网关，实现上述所有功能


大厂：BAT，京东，美团，滴滴之类的，自研网关都是基于netty等技术自研网关；nignx+lua（Tengine）来做，封装网关的功能


中小型公司：Spring cloud技术栈主要使用zuul，如果是dubbo等技术栈，有点采用kong等网关，也可以直接不用网关，很多公司压根儿没用网关，比如系统比较简单，十几个微服务，每个微服务部署一两台机器，前端或者客户端直接请求到Nignx去，Nignx负责请求负载均衡转发到不同的机器上（直接配置si了，什么请求到什么机器，都一一配置好了），毕竟不是什么公司都具备研发能力


zull：基于Java开发，核心网关功能都比较简单，但是比如灰度发布，限流，动态路由之类的，很多需要自己二次开发


Kong：依托于nginx实现，OpResty，lua实现的模块，现成的的一些插件，可以直接使用




 Zuul（Servlet，Java）：高并发能力不强，部署到一些机器上去，还要基于Tomcat部署，Springboot用tomcat把网关系统跑起来；好处是Java语言开发，可以直接把控源码，可以做二次开发封装各种需要的功能

Nginx（Kong，Nignix+lua）：nginx抗高并发能力很强，少数机器部署一下，就可以抗住很高的并发，坏处：精通Nginx源码，很难，C语言写的，很难说从nginx内核层面去做一些二次开发和源码定制

Java技术栈为主的大厂，很多其实用Java，servlet，netty开发高并发，高性能的网关系统，自己可以把控一切