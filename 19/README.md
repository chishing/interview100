
zk，一般来说还好，服务注册和发现，都是很快的

# 如果是springboot 1.5左右的
eureka，必须优化参数

eureka.server.responseCacheUpdateIntervalMs = 3000 eureka.client.registryFetchIntervalSeconds = 30000

eureka.client.leaseRenewalIntervalInSeconds = 30 eureka.server.evictionIntervalTimerInMs = 60000 eureka.instance.leaseExpirationDurationInSeconds = 90

服务发现的时效性变成秒级，几秒钟可以感知服务的上线和下线


# 如果是springboot 2.x的可以这样配置, 我自己找了不少资料 
 
### 注册中心
```
eureka:
  instance:
    hostname: localhost
    lease-expiration-duration-in-seconds: 9 # 表示eureka server至上一次收到client的心跳之后，等待下一次心跳的超时时间，在这个时间内若没收到下一次心跳，则将移除该instance
    lease-renewal-interval-in-seconds: 3 # 3秒心跳给注册中心
  client:
    registerWithEureka: false # #是否将自己注册到eureka中,false：不注册
    fetchRegistry: false  # 是否从eureka中检索服务
    serviceUrl:
      defaultZone: http://localhost:8761/eureka/
    registry-fetch-interval-seconds: 3 # 3秒从注册中心拉去一次注册表, 属于client端的设置
  server:
    enableSelfPreservation: false # 关闭eureka的自我保护机制
    response-cache-update-interval-ms: 1000 #1秒 设置CacheUpdateTask的调度时间间隔，用于从readWriteCacheMap更新数据到readOnlyCacheMap，仅仅在eureka.server.use-read-only-response-cache为true的时候才生效
    eviction-interval-timer-in-ms: 6000 # 6秒内 指定EvictionTask定时任务的调度频率，用于剔除过期的实例 其实我也不知道是从注册表剔除还是从readWrite剔除
    use-read-only-response-cache: true
```

### client端 比如zuul 或者其他服务
```eureka:
     instance:
       hostname: localhost
       lease-expiration-duration-in-seconds: 9 # 表示eureka server至上一次收到client的心跳之后，等待下一次心跳的超时时间，在这个时间内若没收到下一次心跳，则将移除该instance
       lease-renewal-interval-in-seconds: 3 # 3秒心跳给注册中心
     client:
       serviceUrl:
         defaultZone: http://localhost:8761/eureka
       registry-fetch-interval-seconds: 3 # 3秒从注册中心拉去一次注册表, 属于client端的设置
```